CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Micro Path module allows the creation of separate path aliases per micro
site for entities created. 
The path aliases per micro site are implemented as separate "micro_path" content
entities. It is possible to create, edit or delete path alias per micro site
manually.

REQUIREMENTS
------------

This module requires the modules:
  * path (Core)
  * micro_site

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.


CONFIGURATION
-------------


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------

Current maintainers:
 * flocondetoile - https://drupal.org/u/flocondetoile
