<?php

namespace Drupal\micro_path;

final class MicroPathFields {

  /**
   * Name of the field for additional sites for entity.
   */
  const FIELD_SITES = 'field_sites';

  /**
   * Name of the field to publish entity on all sites.
   */
  const FIELD_SITES_ALL = 'field_sites_all';

  /**
   * Name of the field to disable main site canonical url.
   */
  const FIELD_DISABLE_CANONICAL_URL = 'field_disable_canonical_url';
}
